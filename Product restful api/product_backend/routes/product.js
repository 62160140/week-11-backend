const express = require('express')
const router = express.Router()
const Product = require('../models/Product')

/* ----------------------- get all ---------------------- */
const getProducts = async function (req, res, next) {
  //! ต้องใส่ awiat
  try {
    const products = await Product.find({}).exec()
    console.log(products)
    res.status(200).json(products)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}
/* ---------------------- get by id --------------------- */
const getProduct = async function (req, res, next) {
  const id = req.params.id
  try {
    const product = await Product.findById(id).exec()
    res.status(200).json(product)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}
/* --------------------- add product -------------------- */
const addProducuts = async function (req, res, next) {
  const newProduct = new Product({
    name: req.body.name,
    price: parseFloat(req.body.price)
  })
  try {
    await newProduct.save()
    res.status(201).json(newProduct)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}
/* ------------------- update  product ------------------ */
const updateProducut = async function (req, res, next) {
  const productId = req.params.id
  try {
    // เอา มาก่อนแล้วค่อยมาอัพเดท
    const product = await Product.findById(productId)
    product.name = req.body.name
    product.price = parseFloat(req.body.price)
    await product.save()
    res.status(200).send(product)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}
/* ------------------- delete product ------------------- */
const deleteProducut = async function (req, res, next) {
  const index = req.params.id
  try {
    const deletedProduct = await Product.findByIdAndDelete(index).exec()
    res.status(200).send(
      deletedProduct
    )
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

/* ----------- ต่างกันที่ method get กับ post ----------- */
/* ------------------- get All Product ------------------ */
router.get('/', getProducts)
/* ------------------- get  Product id ------------------- */
router.get('/:id', getProduct)
/* --------------------- add Product -------------------- */
router.post('/', addProducuts)
/* --------------------- editProduct -------------------- */
router.put('/:id', updateProducut)
/* -------------------- deleteProduct ------------------- */
router.delete('/:id', deleteProducut)

module.exports = router
