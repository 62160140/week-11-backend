const express = require('express')
const router = express.Router()
const User = require('../models/User')

/* ----------------------- get all ---------------------- */
const getUsers = async function (req, res, next) {
  //! ต้องใส่ awiat
  try {
    const users = await User.find({}).exec()
    console.log(users)
    res.status(200).json(users)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}
/* ---------------------- get by id --------------------- */
const getUser = async function (req, res, next) {
  const id = req.params.id
  try {
    const user = await User.findById(id).exec()
    res.status(200).json(user)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}
/* --------------------- add user -------------------- */
const addUsers = async function (req, res, next) {
  const newUser = new User({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}
/* ------------------- update  user ------------------ */
const updateUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    // เอา มาก่อนแล้วค่อยมาอัพเดท
    const user = await User.findById(userId)
    user.username = req.body.username
    user.password = req.body.password
    user.roles = req.body.roles
    await user.save()
    res.status(200).send(user)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}
/* ------------------- delete user ------------------- */
const deleteUser = async function (req, res, next) {
  const index = req.params.id
  try {
    const deletedUser = await User.findByIdAndDelete(index).exec()
    res.status(200).send(
      deletedUser
    )
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}

/* ----------- ต่างกันที่ method get กับ post ----------- */
/* ------------------- get All User ------------------ */
router.get('/', getUsers)
/* ------------------- get  User id ------------------- */
router.get('/:id', getUser)
/* --------------------- add User -------------------- */
router.post('/', addUsers)
/* --------------------- editUser -------------------- */
router.put('/:id', updateUser)
/* -------------------- deleteUser ------------------- */
router.delete('/:id', deleteUser)

module.exports = router
