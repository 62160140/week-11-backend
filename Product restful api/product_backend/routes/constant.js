const ROLE = {
  ADMIN: 'ADMIN',
  // adminหน่วยงาน
  LOCAL_ADMIN: 'LOCAL_ADMIN',
  USER: 'USER'
}

module.exports = { ROLE }
